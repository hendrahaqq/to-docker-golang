FROM golang:1.15.2  

COPY mediaService /app

COPY exec.sh /app

WORKDIR /app

RUN ["chmod", "+x", "/app/exec.sh"]

ENTRYPOINT [ "/app/exec.sh" ]